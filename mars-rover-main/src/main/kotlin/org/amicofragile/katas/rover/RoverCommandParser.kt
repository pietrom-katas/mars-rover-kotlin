package org.amicofragile.katas.rover

class RoverCommandParser {
    fun parse(input: String): List<RoverCommand> {
        return input.split(' ').map(::create)
    }

    private val commands = mapOf<String, RoverCommand>(
            "F" to Forward(),
            "B" to Backward(),
            "L" to TurnLeft(),
            "R" to TurnRight()
    )

    private fun create(cmd: String) : RoverCommand = commands[cmd]!!
}