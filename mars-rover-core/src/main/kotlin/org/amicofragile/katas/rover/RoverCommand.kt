package org.amicofragile.katas.rover

interface RoverCommand {
    fun execute(position: Position, direction: Direction): Pair<Position, Direction>
}
