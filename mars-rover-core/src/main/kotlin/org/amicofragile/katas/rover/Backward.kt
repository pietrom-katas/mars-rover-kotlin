package org.amicofragile.katas.rover

class Backward : RoverCommand {
    override fun execute(position: Position, direction: Direction): Pair<Position, Direction> {
        return Pair(direction.backward(position), direction)
    }
}