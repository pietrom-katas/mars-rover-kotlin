package org.amicofragile.katas.rover

class Hello {
    fun sayHello(to: String) = "Hello, ${to}!"
}