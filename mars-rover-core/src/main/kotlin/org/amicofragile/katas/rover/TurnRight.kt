package org.amicofragile.katas.rover

class TurnRight : RoverCommand {
    override fun execute(position: Position, direction: Direction): Pair<Position, Direction> {
        return Pair(position, direction.right())
    }
}