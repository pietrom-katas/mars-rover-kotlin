package org.amicofragile.katas.rover

class Forward : RoverCommand {
    override fun execute(position: Position, direction: Direction): Pair<Position, Direction> {
        return Pair(direction.forward(position), direction)
    }
}