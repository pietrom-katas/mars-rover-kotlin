package org.amicofragile.katas.rover

data class Planet(val name: String, val width: Int, val height: Int) {
    fun wrap(pos: Position): Position  = Position((pos.x + width) % width, (pos.y + height) % height)
}