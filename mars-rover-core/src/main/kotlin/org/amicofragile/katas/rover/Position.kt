package org.amicofragile.katas.rover

data class Position(val x: Int, val y: Int) {
}