package org.amicofragile.katas.rover

class TurnLeft : RoverCommand {
    override fun execute(position: Position, direction: Direction): Pair<Position, Direction> {
        return Pair(position, direction.left())
    }
}